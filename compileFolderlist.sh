#!/bin/sh
cd "$1"
allFolders=`ls -d */`
> folderList.txt
for curFile in $allFolders
do   
   s=`echo "$curFile" | sed 's/.$//'`
   echo "$s," >> folderList.txt
done
truncate -s-2 folderList.txt
